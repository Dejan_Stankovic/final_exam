import pandas as pd
from os import walk, remove, path


class Converter:
    def __init__(self, src_reports_folder_path="reports/"):
        self.src_reports_folder_path = src_reports_folder_path

    def convert(self):
        """
        Convert all reports to xlsx files
        """
        client_df = None
        server_df = None
        server_name = ""
        client_name = ""
        client_file_path = ""
        server_file_path = ""
        for root, _, files in walk(self.src_reports_folder_path):
            for file in files:
                if file.endswith(".csv"):
                    file_path = root + "/" + file
                    df = pd.read_csv(file_path)
                    if "client" in file.lower():
                        client_df = df
                        client_name = file
                        client_file_path = file_path
                    else:
                        server_df = df
                        server_name = file
                        server_file_path = file_path
        if server_name == "" and client_name == "":
            raise Exception("No reports found")

        if not path.exists(self.src_reports_folder_path + "report.xlsx"):
            empty_df = pd.DataFrame()
            empty_df.to_excel(
                self.src_reports_folder_path + "report.xlsx",
                sheet_name="Final Exam",
                index=False,
            )
        client_words = client_name.split(".")
        server_words = server_name.split(".")
        with pd.ExcelWriter(
            self.src_reports_folder_path + "report.xlsx",
            mode="a",
            engine="openpyxl",
        ) as writer:
            if client_df is not None:
                client_df.to_excel(
                    writer,
                    sheet_name=f"{'.'.join(client_words[-3:-1])} ({len(writer.sheets)})",
                    index=False,
                )
                remove(client_file_path)
            if server_df is not None:
                server_df.to_excel(
                    writer,
                    sheet_name=f"{'.'.join(server_words[-3:-1])} ({len(writer.sheets)})",
                    index=False,
                )
                remove(server_file_path)


if __name__ == "__main__":
    converter = Converter()
    converter.convert()
