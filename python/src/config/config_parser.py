import configparser


class ConfigParser:
    def __init__(self, config_file="config.ini"):
        self.config = configparser.ConfigParser()
        self.config.read(config_file)

    def get_value(self, section, option):
        try:
            value = self.config.get(section, option)
            return value
        except (configparser.NoSectionError, configparser.NoOptionError):
            return None

    def set_value(self, section, option, value):
        if not self.config.has_section(section):
            self.config.add_section(section)
        self.config.set(section, option, value)

    def save(self, output_file="config.ini"):
        with open(output_file, "w") as file:
            self.config.write(file)
