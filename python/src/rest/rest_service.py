import logging
from flask import Flask, request
from config import ConfigParser
from model import TestData
from datetime import datetime
import csv
import os

app = Flask(__name__)
logging.getLogger("werkzeug").setLevel(logging.ERROR)

config = ConfigParser()
SERVER_REPORT_FIELDNAMES = config.get_value("report-columns", "SERVER").split(",")
NUM_REQUESTS = int(config.get_value("client", "NUM_REQUESTS"))

csv_file = open(f"reports/python/{os.path.basename(__file__)}.csv", "a")
csv_writer = csv.DictWriter(csv_file, fieldnames=SERVER_REPORT_FIELDNAMES)
if csv_file.tell() == 0:
    csv_writer.writeheader()

processed_requests = 0


def measure_request_time(func):
    def wrapper(*args, **kwargs):
        request_started_at = datetime.now()
        func_result, times = func(*args, **kwargs)
        request_ended_at = datetime.now()

        processing_started_at, processed_at, sending_started_at = times

        receiving_time = (processing_started_at - request_started_at).total_seconds()
        processing_time = (processed_at - processing_started_at).total_seconds()
        sending_time = (request_ended_at - sending_started_at).total_seconds()
        total_time = (request_ended_at - request_started_at).total_seconds()

        result_dict = {
            SERVER_REPORT_FIELDNAMES[0]: receiving_time,
            SERVER_REPORT_FIELDNAMES[1]: processing_time,
            SERVER_REPORT_FIELDNAMES[2]: sending_time,
            SERVER_REPORT_FIELDNAMES[3]: total_time,
        }
        csv_writer.writerow(result_dict)

        global processed_requests
        processed_requests += 1
        if processed_requests == NUM_REQUESTS:
            csv_file.close()

        return func_result

    return wrapper


@app.route("/api/test", methods=["POST"])
@measure_request_time
def receive_message():
    data = request.get_json()

    processing_started_at = datetime.now()
    test_data = TestData.from_json(data)
    test_data.array.sort()
    test_data.string = test_data.string[::-1]
    processed_at = datetime.now()

    sending_started_at = datetime.now()
    response = test_data.to_json()
    times = (processing_started_at, processed_at, sending_started_at)
    return response, times
