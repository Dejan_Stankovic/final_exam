from werkzeug.serving import run_simple
from config import ConfigParser
from rest import app


config = ConfigParser()
HOST = config.get_value("rest-service", "HOST")
PORT = config.get_value("rest-service", "PORT")

if __name__ == "__main__":
    run_simple(
        HOST,
        int(PORT),
        app,
    )
