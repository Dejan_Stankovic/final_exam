import requests
from model import TestData
from config import ConfigParser
from datetime import datetime
import csv
import os

config = ConfigParser()
HEADERS = {"Content-type": "application/json"}
HOST = config.get_value("rest-service", "HOST")
PORT = config.get_value("rest-service", "PORT")
NUM_REQUESTS = int(config.get_value("client", "NUM_REQUESTS"))
CLIENT_REPORT_FIELDNAMES = config.get_value("report-columns", "CLIENT").split(",")

csv_file = open(f"reports/python/{os.path.basename(__file__)}.csv", "w")
csv_writer = csv.DictWriter(csv_file, fieldnames=CLIENT_REPORT_FIELDNAMES)
csv_writer.writeheader()


class RestClient:
    def run_client(self):
        # Measure the time taken to send the requests
        client_started_at = datetime.now()
        url = f"http://{HOST}:{PORT}/api/test"

        for _ in range(NUM_REQUESTS):
            creating_message_started_at = datetime.now()
            test_data = TestData()
            test_data.add_random_string()
            test_data.add_big_array()
            message = test_data.to_json()
            creating_message_ended_at = datetime.now()

            request_started_at = datetime.now()
            requests.post(url, message, headers=HEADERS)
            request_ended_at = datetime.now()

            creating_message_time = (
                creating_message_ended_at - creating_message_started_at
            ).total_seconds()
            request_time = (request_ended_at - request_started_at).total_seconds()
            total_time = (
                request_ended_at - creating_message_started_at
            ).total_seconds()

            result_dict = {
                CLIENT_REPORT_FIELDNAMES[0]: creating_message_time,
                # we have only one value for sending a post request and receiving a response
                CLIENT_REPORT_FIELDNAMES[2]: request_time,
                CLIENT_REPORT_FIELDNAMES[3]: total_time,
            }
            csv_writer.writerow(result_dict)

        client_ended_at = datetime.now()
        execution_time = (client_ended_at - client_started_at).total_seconds()

        csv_writer.writerow({CLIENT_REPORT_FIELDNAMES[4]: execution_time})
        csv_file.close()


if __name__ == "__main__":
    client = RestClient()
    client.run_client()
