import pika
from config import ConfigParser
from model import TestData
from datetime import datetime
import csv
import os


class MQServer:
    def __init__(self):
        self.config = ConfigParser()

        self.host = self.config.get_value("rabbitmq", "HOST")
        self.port = self.config.get_value("rabbitmq", "PORT")
        self.queue_1 = self.config.get_value("rabbitmq", "QUEUE_1")
        self.queue_2 = self.config.get_value("rabbitmq", "QUEUE_2")
        self.exchange_1 = self.config.get_value("rabbitmq", "EXCHANGE_1")
        self.exchange_2 = self.config.get_value("rabbitmq", "EXCHANGE_2")
        self.routing_key = self.config.get_value("rabbitmq", "ROUTING_KEY")
        self.virtual_host = self.config.get_value("rabbitmq", "VIRTUAL_HOST")
        self.username = self.config.get_value("rabbitmq", "USERNAME")
        self.password = self.config.get_value("rabbitmq", "PASSWORD")
        self.exchange_type = self.config.get_value("rabbitmq", "EXCHANGE_TYPE")
        self.server_report_fieldnames = self.config.get_value(
            "report-columns", "SERVER"
        ).split(",")

        self.csv_file = open(f"reports/python/{os.path.basename(__file__)}.csv", "w")
        self.csv_writer = csv.DictWriter(
            self.csv_file, fieldnames=self.server_report_fieldnames
        )
        self.csv_writer.writeheader()

        # Connect to RabbitMQ server
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.host,
                port=self.port,
                virtual_host=self.virtual_host,
                credentials=pika.PlainCredentials(self.username, self.password),
            )
        )
        channel_1 = self.connection.channel()
        channel_2 = self.connection.channel()

        # Declare the exchange
        channel_1.exchange_declare(
            exchange=self.exchange_1, exchange_type=self.exchange_type
        )
        channel_2.exchange_declare(
            exchange=self.exchange_2, exchange_type=self.exchange_type
        )

        # Declare the queues
        channel_1.queue_declare(queue=self.queue_1)
        channel_2.queue_declare(queue=self.queue_2)

        channel_1.queue_bind(
            queue=self.queue_1, exchange=self.exchange_1, routing_key=self.routing_key
        )
        channel_2.queue_bind(
            queue=self.queue_2, exchange=self.exchange_2, routing_key=self.routing_key
        )

        # Start consuming messages from the queue
        channel_1.basic_consume(
            queue=self.queue_1, on_message_callback=self.callback, auto_ack=True
        )

        self.channel_1 = channel_1
        self.channel_2 = channel_2

    def callback(self, ch, method, properties, body):
        message = body.decode()
        if message != "exit!":
            processing_started_at = datetime.now()
            test_data = TestData.from_json(message)
            test_data.array.sort()
            test_data.string = test_data.string[::-1]
            processed_at = datetime.now()
            message = test_data.to_json()
            self.channel_2.basic_publish(
                exchange=self.exchange_2,
                routing_key=self.routing_key,
                body=message,
            )
            sent_at = datetime.now()

            processing_time = (processed_at - processing_started_at).total_seconds()
            sending_time = (sent_at - processed_at).total_seconds()
            total_time = (sent_at - processing_started_at).total_seconds()

            result_dict = {
                # we don't have receiving time here
                self.server_report_fieldnames[1]: processing_time,
                self.server_report_fieldnames[2]: sending_time,
                self.server_report_fieldnames[3]: total_time,
            }
            self.csv_writer.writerow(result_dict)
        else:
            message = "exit!"
            self.channel_2.basic_publish(
                exchange=self.exchange_2,
                routing_key=self.routing_key,
                body=message,
            )
            ch.stop_consuming()

    def consume_message(self):
        print("Waiting for messages. To exit, press CTRL+C")

        # Start consuming (blocking)
        self.channel_1.start_consuming()


if __name__ == "__main__":
    server = MQServer()

    server_consume_started_at = datetime.now()
    server.consume_message()
    server_consume_ended_at = datetime.now()

    execution_time = (
        server_consume_ended_at - server_consume_started_at
    ).total_seconds()
    server.csv_writer.writerow({server.server_report_fieldnames[4]: execution_time})
    server.csv_file.close()
