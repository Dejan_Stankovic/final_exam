import pika
from config import ConfigParser
from model import TestData
from datetime import datetime
import csv
import os


class MQClient:
    def __init__(self):
        self.config = ConfigParser()

        self.host = self.config.get_value("rabbitmq", "HOST")
        self.port = self.config.get_value("rabbitmq", "PORT")
        self.queue_1 = self.config.get_value("rabbitmq", "QUEUE_1")
        self.queue_2 = self.config.get_value("rabbitmq", "QUEUE_2")
        self.exchange_1 = self.config.get_value("rabbitmq", "EXCHANGE_1")
        self.exchange_2 = self.config.get_value("rabbitmq", "EXCHANGE_2")
        self.routing_key = self.config.get_value("rabbitmq", "ROUTING_KEY")
        self.virtual_host = self.config.get_value("rabbitmq", "VIRTUAL_HOST")
        self.username = self.config.get_value("rabbitmq", "USERNAME")
        self.password = self.config.get_value("rabbitmq", "PASSWORD")
        self.exchange_type = self.config.get_value("rabbitmq", "EXCHANGE_TYPE")

        self.num_requests = int(self.config.get_value("client", "NUM_REQUESTS"))

        self.client_report_fieldnames = self.config.get_value(
            "report-columns", "CLIENT"
        ).split(",")

        self.csv_file = open(f"reports/python/{os.path.basename(__file__)}.csv", "w")
        self.csv_writer = csv.DictWriter(
            self.csv_file, fieldnames=self.client_report_fieldnames
        )
        self.csv_writer.writeheader()

        # Connect to the RabbitMQ server
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.host,
                port=self.port,
                virtual_host=self.virtual_host,
                credentials=pika.PlainCredentials(self.username, self.password),
            )
        )
        channel_1 = self.connection.channel()
        channel_2 = self.connection.channel()

        # Declare the exchanges
        channel_1.exchange_declare(
            exchange=self.exchange_1, exchange_type=self.exchange_type
        )
        channel_2.exchange_declare(
            exchange=self.exchange_2, exchange_type=self.exchange_type
        )

        # Declare the queues
        channel_1.queue_declare(queue=self.queue_1)
        channel_2.queue_declare(queue=self.queue_2)

        channel_1.queue_bind(
            queue=self.queue_1, exchange=self.exchange_1, routing_key=self.routing_key
        )
        channel_2.queue_bind(
            queue=self.queue_2, exchange=self.exchange_2, routing_key=self.routing_key
        )

        # Start consuming messages from the queue
        channel_2.basic_consume(
            queue=self.queue_2, on_message_callback=self.callback, auto_ack=True
        )

        self.channel_1 = channel_1
        self.channel_2 = channel_2

    def callback(self, ch, method, properties, body):
        message = body.decode()
        if message == "exit!":
            ch.stop_consuming()

    def consume_messages(self):
        print("Waiting for messages. To exit, press CTRL+C")

        # Start consuming (blocking)
        self.channel_2.start_consuming()

    def publish_messages(self):
        for _ in range(self.num_requests):
            single_meesage_creation_started_at = datetime.now()
            test_data = TestData()
            test_data.add_random_string()
            test_data.add_big_array()
            message = test_data.to_json()
            single_meesage_creation_ended_at = datetime.now()

            self.channel_1.basic_publish(
                exchange=self.exchange_1,
                routing_key=self.routing_key,
                body=message,
            )
            single_meesage_sent_at = datetime.now()

            creating_message_time = (
                single_meesage_creation_ended_at - single_meesage_creation_started_at
            ).total_seconds()
            sending_message_time = (
                single_meesage_sent_at - single_meesage_creation_ended_at
            ).total_seconds()
            total_time = (
                single_meesage_sent_at - single_meesage_creation_started_at
            ).total_seconds()

            result_dict = {
                self.client_report_fieldnames[0]: creating_message_time,
                self.client_report_fieldnames[1]: sending_message_time,
                # we are not receiving any response from the server in this part
                self.client_report_fieldnames[3]: total_time,
            }
            self.csv_writer.writerow(result_dict)
        else:
            message = "exit!"
            self.channel_1.basic_publish(
                exchange=self.exchange_1,
                routing_key=self.routing_key,
                body=message,
            )


if __name__ == "__main__":
    client = MQClient()

    publishing_started_at = datetime.now()

    client.publish_messages()

    client.consume_messages()

    consuming_finished_at = datetime.now()

    total_execution_time = (
        consuming_finished_at - publishing_started_at
    ).total_seconds()
    client.csv_writer.writerow(
        {client.client_report_fieldnames[4]: total_execution_time}
    )
    client.csv_file.close()
    client.connection.close()
