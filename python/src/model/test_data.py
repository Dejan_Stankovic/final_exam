import json
from random import randint, choice
from config import ConfigParser
import xml.etree.ElementTree as ET
import avro.schema
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter
import io


config = ConfigParser()


class TestData:
    def __init__(self):
        self.string = ""
        self.array = []

    def add_random_string(self):
        DATA_ARRAY_LEN = int(config.get_value("model", "DATA_ARRAY_LEN"))
        self.string = "".join(
            choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            for _ in range(DATA_ARRAY_LEN)
        )

    def add_big_array(self):
        DATA_ARRAY_MAX_VALUE = int(config.get_value("model", "DATA_ARRAY_MAX_VALUE"))
        DATA_ARRAY_LEN = int(config.get_value("model", "DATA_ARRAY_LEN"))
        self.array = [randint(0, DATA_ARRAY_MAX_VALUE) for _ in range(DATA_ARRAY_LEN)]

    def to_json(self):
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, json_data):
        if type(json_data) is bytes:
            json_string = json_data.decode()
            json_dict = json.loads(json_string)
        elif type(json_data) is dict:
            json_dict = json_data
        elif type(json_data) is str:
            json_dict = json.loads(json_data)
        else:
            raise TypeError(
                f"Can't convert {type(json_data)} to {type(dict)} or {type(str)}"
            )
        test_data = cls()
        test_data.string = json_dict.get("string", "")
        test_data.array = json_dict.get("array", [])
        return test_data

    def to_xml(self):
        root = ET.Element("root")
        string = ET.SubElement(root, "string")
        string.text = self.string
        array = ET.SubElement(root, "array")
        for x in self.array:
            array.append(ET.Element("array"))
            array[-1].text = str(x)
        return ET.tostring(root, encoding="unicode")

    @classmethod
    def from_xml(cls, xml_data):
        if type(xml_data) is bytes:
            xml_string = xml_data.decode()
        elif type(xml_data) is str:
            xml_string = xml_data
        else:
            raise TypeError(f"Can't convert {type(xml_data)} to {type(str)}")
        test_data = cls()
        root = ET.fromstring(xml_string)
        test_data.string = root.find("string").text
        arr = root.find("array")
        test_data.array = [int(x.text) for x in arr.findall("array")]
        return test_data

    def to_avro(self):
        schema = avro.schema.parse(open("testData.avsc").read())
        output_stream = io.BytesIO()
        writer = DataFileWriter(output_stream, DatumWriter(), schema)
        generic_record = {"string": self.string, "array": self.array}
        writer.append(generic_record)
        writer.flush()
        bytes_data = output_stream.getvalue()
        writer.close()
        return bytes_data

    @classmethod
    def from_avro(cls, avro_bytes):
        schema = avro.schema.parse(open("testData.avsc").read())
        test_data_datum_reader = DatumReader(schema)
        input_stream = io.BytesIO(avro_bytes)
        reader = DataFileReader(input_stream, test_data_datum_reader)

        for data in reader:
            test_data = cls()
            test_data.string = data.get("string", "")
            test_data.array = data.get("array", [])
            return test_data
