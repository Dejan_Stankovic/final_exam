from random import randint, choice

from config import ConfigParser
from .testDataProto_pb2 import TestDataProto


config = ConfigParser()


class TestDataPB:
    def __init__(self):
        self.test_data = TestDataProto()

    def add_random_string(self):
        DATA_ARRAY_LEN = int(config.get_value("model", "DATA_ARRAY_LEN"))
        self.test_data.string = "".join(
            choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            for _ in range(DATA_ARRAY_LEN)
        )

    def add_big_array(self):
        DATA_ARRAY_MAX_VALUE = int(config.get_value("model", "DATA_ARRAY_MAX_VALUE"))
        DATA_ARRAY_LEN = int(config.get_value("model", "DATA_ARRAY_LEN"))
        self.test_data.array.extend(
            [randint(0, DATA_ARRAY_MAX_VALUE) for _ in range(DATA_ARRAY_LEN)]
        )
