import socket
import csv
import os
from datetime import datetime

from config import ConfigParser
from model import TestData
from model import TestDataPB

config = ConfigParser()
HOST = config.get_value("socket-server", "HOST")
PORT = int(config.get_value("socket-server", "PORT"))
BUFFER_SIZE = int(config.get_value("socket-server", "BUFFER_SIZE"))
NUM_REQUESTS = int(config.get_value("client", "NUM_REQUESTS"))
CLIENT_REPORT_FIELDNAMES = config.get_value("report-columns", "CLIENT").split(",")
SERIALIZATION_TYPE = config.get_value("model", "SERIALIZATION_TYPE")

csv_file = open(f"reports/python/{os.path.basename(__file__)}.csv", "w")
csv_writer = csv.DictWriter(csv_file, fieldnames=CLIENT_REPORT_FIELDNAMES)
csv_writer.writeheader()


class SocketClient:
    def run_client(self):
        # Create a socket object
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect to the server
        server_address = (HOST, PORT)
        client_socket.connect(server_address)

        client_started_at = datetime.now()
        for _ in range(NUM_REQUESTS):
            # Send data to the server
            creating_message_started_at = datetime.now()
            test_data = (
                TestData() if not "PROTO" == SERIALIZATION_TYPE else TestDataPB()
            )
            test_data.add_random_string()
            test_data.add_big_array()

            message_bytes = bytes()
            if "JSON" == SERIALIZATION_TYPE:
                message_bytes = test_data.to_json().encode()
            elif "XML" == SERIALIZATION_TYPE:
                message_bytes = test_data.to_xml().encode()
            elif "AVRO" == SERIALIZATION_TYPE:
                message_bytes = test_data.to_avro()
            elif "PROTO" == SERIALIZATION_TYPE:
                message_bytes = test_data.test_data.SerializeToString()

            creating_message_ended_at = datetime.now()
            sending_started_at = datetime.now()
            client_socket.send(message_bytes)
            client_socket.send("sent!".encode())
            sent_at = datetime.now()

            # Receive the server's response
            response = bytearray()
            sent_encoded = "sent!".encode()
            while True:
                data = client_socket.recv(BUFFER_SIZE)
                if data[-len(sent_encoded) :] == sent_encoded:
                    response.extend(data[: -len(sent_encoded)])
                    break
                response.extend(data)

            if "JSON" == SERIALIZATION_TYPE:
                test_data2 = TestData.from_json(response.decode())
            elif "XML" == SERIALIZATION_TYPE:
                test_data2 = TestData.from_xml(response.decode())
            elif "PROTO" == SERIALIZATION_TYPE:
                test_data2 = TestDataPB()
                test_data2.test_data.ParseFromString(bytes(response))
                test_data = test_data2.test_data
            elif "AVRO" == SERIALIZATION_TYPE:
                test_data2 = TestData.from_avro(response)

            received_at = datetime.now()

            creating_message_time = (
                creating_message_ended_at - creating_message_started_at
            ).total_seconds()
            sending_message_time = (sent_at - sending_started_at).total_seconds()
            receiving_response_time = (received_at - sent_at).total_seconds()
            total_time = (received_at - creating_message_started_at).total_seconds()

            result_dict = {
                CLIENT_REPORT_FIELDNAMES[0]: creating_message_time,
                CLIENT_REPORT_FIELDNAMES[1]: sending_message_time,
                CLIENT_REPORT_FIELDNAMES[2]: receiving_response_time,
                CLIENT_REPORT_FIELDNAMES[3]: total_time,
            }
            csv_writer.writerow(result_dict)
        else:
            client_socket.send("exit!".encode())
            client_socket.close()

        client_ended_at = datetime.now()
        client_execution_time = (client_ended_at - client_started_at).total_seconds()
        csv_writer.writerow({CLIENT_REPORT_FIELDNAMES[4]: client_execution_time})
        csv_file.close()

        # Close the client socket
        client_socket.close()


if __name__ == "__main__":
    client = SocketClient()
    client.run_client()
