import socket
import csv
import os
from datetime import datetime

from config import ConfigParser
from model import TestData
from model import TestDataPB


config = ConfigParser()
HOST = config.get_value("socket-server", "HOST")
PORT = int(config.get_value("socket-server", "PORT"))
BUFFER_SIZE = int(config.get_value("socket-server", "BUFFER_SIZE"))
SERVER_REPORT_FIELDNAMES = config.get_value("report-columns", "SERVER").split(",")
SERIALIZATION_TYPE = config.get_value("model", "SERIALIZATION_TYPE")

csv_file = open(f"reports/python/{os.path.basename(__file__)}.csv", "w")
csv_writer = csv.DictWriter(csv_file, fieldnames=SERVER_REPORT_FIELDNAMES)
csv_writer.writeheader()


class SocketServer:
    def run_server(self):
        # Create a socket object
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Bind the socket to a specific address and port
        server_address = (HOST, PORT)
        server_socket.bind(server_address)

        # Listen for incoming connections
        server_socket.listen(10)
        print("Server is listening on {}:{}".format(*server_address))

        # Accept a client connection
        client_socket, client_address = server_socket.accept()
        print("Accepted connection from {}:{}".format(*client_address))

        socket_started_at = datetime.now()
        while True:
            # Receive data from the client
            receiving_started_at = datetime.now()
            message = bytearray()
            while True:
                data = client_socket.recv(BUFFER_SIZE)
                sent_encoded = "sent!".encode()
                exit_encoded = "exit!".encode()
                if (
                    data[-len(sent_encoded) :] == sent_encoded
                    or data[-len(exit_encoded) :] == exit_encoded
                ):
                    if data[-len(sent_encoded) :] == sent_encoded:
                        message.extend(data[: -len(sent_encoded)])
                    break
                else:
                    message.extend(data)
            received_at = datetime.now()

            if data[-len(exit_encoded) :] == exit_encoded:
                client_socket.close()
                server_socket.close()
                break

            processing_started_at = datetime.now()

            if "JSON" == SERIALIZATION_TYPE:
                test_data = TestData.from_json(message.decode())
            elif "XML" == SERIALIZATION_TYPE:
                test_data = TestData.from_xml(message.decode())
            elif "PROTO" == SERIALIZATION_TYPE:
                test_data2 = TestDataPB()
                test_data2.test_data.ParseFromString(bytes(message))
                test_data = test_data2.test_data
            elif "AVRO" == SERIALIZATION_TYPE:
                test_data = TestData.from_avro(message)

            test_data.array.sort()
            test_data.string = test_data.string[::-1]
            processed_at = datetime.now()

            sending_started_at = datetime.now()

            response_bytes = []
            if "JSON" == SERIALIZATION_TYPE:
                response_bytes = test_data.to_json().encode()
            elif "XML" == SERIALIZATION_TYPE:
                response_bytes = test_data.to_xml().encode()
            elif "PROTO" == SERIALIZATION_TYPE:
                response_bytes = test_data.SerializeToString()
            elif "AVRO" == SERIALIZATION_TYPE:
                response_bytes = test_data.to_avro()

            client_socket.send(response_bytes)
            client_socket.send("sent!".encode())
            sent_at = datetime.now()

            receiving_time = (received_at - receiving_started_at).total_seconds()
            processing_time = (processed_at - processing_started_at).total_seconds()
            sending_time = (sent_at - sending_started_at).total_seconds()
            total_time = (sent_at - receiving_started_at).total_seconds()

            result_dict = {
                SERVER_REPORT_FIELDNAMES[0]: receiving_time,
                SERVER_REPORT_FIELDNAMES[1]: processing_time,
                SERVER_REPORT_FIELDNAMES[2]: sending_time,
                SERVER_REPORT_FIELDNAMES[3]: total_time,
            }
            csv_writer.writerow(result_dict)

        socket_ended_at = datetime.now()
        socket_execution_time = (socket_ended_at - socket_started_at).total_seconds()
        csv_writer.writerow({SERVER_REPORT_FIELDNAMES[4]: socket_execution_time})
        csv_file.close()

        # Close the server socket
        server_socket.close()


if __name__ == "__main__":
    server = SocketServer()
    server.run_server()
