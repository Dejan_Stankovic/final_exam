package config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ConfigParser {

    private Map<String, Map<String, String>> configData;

    public ConfigParser() {
        configData = new HashMap<>();
        this.parse("config.ini");
    }

    public void parse(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            String currentSection = null;

            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || line.startsWith(";")) {
                    // Skip empty lines or comments (lines starting with ';')
                    continue;
                } else if (line.startsWith("[")) {
                    // Found a section
                    currentSection = line.substring(1, line.length() - 1).trim();
                    configData.put(currentSection, new HashMap<>());
                } else if (currentSection != null && line.contains("=")) {
                    // Found an option in a section
                    String[] parts = line.split("=", 2); // Split only on the first '='
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    configData.get(currentSection).put(key, value);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getValue(String section, String option) {
        Map<String, String> sectionData = configData.get(section);
        if (sectionData != null) {
            return sectionData.get(option);
        }
        return null;
    }
}
