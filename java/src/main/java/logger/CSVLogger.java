package logger;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class CSVLogger {

    private String[] fieldNames;
    private CSVWriter writer;

    public CSVLogger(String csvFilePath, boolean append, String[] fieldNames) {
        this.fieldNames = fieldNames;
        this.writer = null;
        try {
            this.writer = new CSVWriter(new FileWriter(csvFilePath, append));
            this.writer.writeNext(fieldNames);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logData(Map<String, String> data) {
        String[] dataArr = new String[this.fieldNames.length];
        for (int i = 0; i < this.fieldNames.length; i++) {
            dataArr[i] = data.get(this.fieldNames[i]);
        }
        this.writer.writeNext(dataArr);
    }

    public void close() {
        try {
            this.writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
