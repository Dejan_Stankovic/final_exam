package socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;
import logger.CSVLogger;

import config.ConfigParser;
import model.TestData;
import model.TestDataPB;

public class SocketClient {

    private static ConfigParser config = new ConfigParser();

    private void runClient() {
        String host = config.getValue("socket-server", "HOST");
        int port = Integer.parseInt(config.getValue("socket-server", "PORT"));
        int bufferSize = Integer.parseInt(config.getValue("socket-server", "BUFFER_SIZE"));
        int numRequests = Integer.parseInt(config.getValue("client", "NUM_REQUESTS"));
        String[] clientReportFieldnames = config.getValue("report-columns", "CLIENT").split(",");
        String serializationType = config.getValue("model", "SERIALIZATION_TYPE");

        String className = this.getClass().getName();
        String csvFile = "reports/java/" + className + ".java.csv";
        CSVLogger logger = new CSVLogger(csvFile, false, clientReportFieldnames);

        try (Socket socket = new Socket(host, port)) {
            // Create input and output streams for communication
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            long clientBeganAt = System.nanoTime();
            for (int i = 0; i < numRequests; i++) {
                // Send data to the server
                long creatingMessageStartedAt = System.nanoTime();
                TestData testData = null;
                TestDataPB testDataPB = null;
                if ("PROTO".equals(serializationType)) {
                    testDataPB = new TestDataPB();
                    testDataPB.addBigArray();
                    testDataPB.addRandomString();
                } else {
                    testData = new TestData();
                    testData.addBigArray();
                    testData.addRandomString();
                }

                byte[] messageBytes = new byte[0];
                if ("JSON".equals(serializationType)) {
                    messageBytes = testData.toJson().getBytes();
                } else if ("XML".equals(serializationType)) {
                    messageBytes = testData.toXml().getBytes();
                } else if ("PROTO".equals(serializationType)) {
                    messageBytes = testDataPB
                            .toByteArray();
                } else if ("AVRO".equals(serializationType)) {
                    messageBytes = testData.toAvro();
                } else {
                    System.err.println("Unknown serialization type: " + serializationType);
                    System.exit(1);
                }

                long creatingMessageEndedAt = System.nanoTime();
                long sendingStartedAt = System.nanoTime();
                outputStream.write(messageBytes);
                outputStream.write("sent!".getBytes());
                long sentAt = System.nanoTime();

                // Read response from the server
                byte[] buffer = new byte[0];
                byte[] currentBuffer;
                int bytesRead;
                byte[] sentBytes = "sent!".getBytes();
                while (true) {
                    currentBuffer = new byte[bufferSize];
                    bytesRead = inputStream.read(currentBuffer);

                    if (endsWithSubarray(currentBuffer, bytesRead, sentBytes)) {
                        if (endsWithSubarray(currentBuffer, bytesRead, sentBytes)) {
                            byte[] extendedBuffer = new byte[buffer.length + bytesRead - sentBytes.length];
                            System.arraycopy(buffer, 0, extendedBuffer, 0, buffer.length);
                            System.arraycopy(currentBuffer, 0, extendedBuffer, buffer.length,
                                    bytesRead - sentBytes.length);
                            buffer = extendedBuffer;
                        }
                        break;
                    } else {
                        byte[] extendedBuffer = new byte[buffer.length + bytesRead];
                        System.arraycopy(buffer, 0, extendedBuffer, 0, buffer.length);
                        System.arraycopy(currentBuffer, 0, extendedBuffer, buffer.length, bytesRead);
                        buffer = extendedBuffer;
                    }
                }
                long receivedAt = System.nanoTime();

                double creatingMessageTime = (creatingMessageEndedAt - creatingMessageStartedAt) / 1_000_000_000.0;
                double sendingTime = (sentAt - sendingStartedAt) / 1_000_000_000.0;
                double receivingTime = (receivedAt - sentAt) / 1_000_000_000.0;
                double totalTime = (receivedAt - creatingMessageStartedAt) / 1_000_000_000.0;

                Map<String, String> resultMap = Map.of(
                        clientReportFieldnames[0], String.valueOf(creatingMessageTime),
                        clientReportFieldnames[1], String.valueOf(sendingTime),
                        clientReportFieldnames[2], String.valueOf(receivingTime),
                        clientReportFieldnames[3], String.valueOf(totalTime));
                logger.logData(resultMap);

            }
            outputStream.write("exit!".getBytes());
            long clientEndedAt = System.nanoTime();
            double clientExecutionTime = (clientEndedAt - clientBeganAt) / 1_000_000_000.0;
            logger.logData(Map.of(clientReportFieldnames[4], String.valueOf(clientExecutionTime)));
            logger.close();
        } catch (IOException e) {
            System.err.println("Client startup failed: " + e.getMessage());
        }
    }

    private boolean endsWithSubarray(byte[] mainArray, int bytesRead, byte[] subArray) {
        if (mainArray.length < subArray.length) {
            return false;
        }

        int subArrayIndex = subArray.length - 1;
        for (int i = bytesRead - 1; subArrayIndex >= 0; i--) {
            if (mainArray[i] != subArray[subArrayIndex]) {
                return false;
            }
            subArrayIndex--;
        }
        return true;
    }

    public static void main(String[] args) {
        SocketClient client = new SocketClient();
        client.runClient();
    }

}
