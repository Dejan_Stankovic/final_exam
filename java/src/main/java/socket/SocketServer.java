package socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import logger.CSVLogger;

import config.ConfigParser;
import model.TestData;
import model.TestDataPB;

public class SocketServer {
    private static ConfigParser config = new ConfigParser();

    private void runServer() {
        String host = config.getValue("socket-server", "HOST");
        int port = Integer.parseInt(config.getValue("socket-server", "PORT"));
        int bufferSize = Integer.parseInt(config.getValue("socket-server", "BUFFER_SIZE"));
        String[] serverReportFieldnames = config.getValue("report-columns", "SERVER").split(",");
        String serializationType = config.getValue("model", "SERIALIZATION_TYPE");

        String className = this.getClass().getName();
        String csvFile = "reports/java/" + className + ".java.csv";
        CSVLogger logger = new CSVLogger(csvFile, false, serverReportFieldnames);
        ServerSocket serverSocket = null;
        try {
            InetSocketAddress address = new InetSocketAddress(host, port);
            serverSocket = new ServerSocket();
            serverSocket.bind(address);
            System.out.println("Server is listening on " + host + ":" + port);

            try (Socket socket = serverSocket.accept()) {
                System.out.println("Accepted connection from " + socket.getInetAddress());
                // Create input and output streams for communication
                InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();

                long serverStartedAt = System.nanoTime();
                while (true) {
                    // Read message from the client
                    long receivingStartedAt = System.nanoTime();
                    byte[] buffer = new byte[0];
                    byte[] currentBuffer;
                    int bytesRead;
                    byte[] sentBytes = "sent!".getBytes();
                    byte[] exitBytes = "exit!".getBytes();

                    while (true) {
                        currentBuffer = new byte[bufferSize];
                        bytesRead = inputStream.read(currentBuffer);

                        if (endsWithSubarray(currentBuffer, bytesRead, sentBytes)
                                || endsWithSubarray(currentBuffer, bytesRead, exitBytes)) {
                            if (endsWithSubarray(currentBuffer, bytesRead, sentBytes)) {
                                byte[] extendedBuffer = new byte[buffer.length + bytesRead - sentBytes.length];
                                System.arraycopy(buffer, 0, extendedBuffer, 0, buffer.length);
                                System.arraycopy(currentBuffer, 0, extendedBuffer, buffer.length,
                                        bytesRead - sentBytes.length);
                                buffer = extendedBuffer;
                            }
                            break;
                        } else {
                            byte[] extendedBuffer = new byte[buffer.length + bytesRead];
                            System.arraycopy(buffer, 0, extendedBuffer, 0, buffer.length);
                            System.arraycopy(currentBuffer, 0, extendedBuffer, buffer.length, bytesRead);
                            buffer = extendedBuffer;
                        }
                    }
                    long receivedAt = System.nanoTime();

                    if (endsWithSubarray(currentBuffer, bytesRead, exitBytes)) {
                        System.out.println("Closing the server!");
                        break;
                    }
                    long processingStartedAt = System.nanoTime();

                    TestData testData = null;
                    TestDataPB testDataPB = null;
                    if ("JSON".equals(serializationType)) {
                        testData = TestData.fromJson(new String(buffer));
                    } else if ("XML".equals(serializationType)) {
                        testData = TestData.fromXml(new String(buffer));
                    } else if ("PROTO".equals(serializationType)) {
                        testDataPB = new TestDataPB(buffer);
                    } else if ("AVRO".equals(serializationType)) {
                        testData = TestData.fromAvro(buffer);
                    } else {
                        System.err.println("Unknown serialization type: " + serializationType);
                        System.exit(1);
                    }
                    if ("PROTO".equals(serializationType)) {
                        Arrays.sort(testDataPB.getArray());
                        testDataPB.setString(
                                new StringBuilder(testDataPB.getString()).reverse().toString());
                    } else {
                        Collections.sort(testData.getArray());
                        testData.setString(
                                new StringBuilder(testData.getString()).reverse().toString());
                    }
                    long processedAt = System.nanoTime();

                    long sendingStartedAt = System.nanoTime();

                    byte[] responseBytes = new byte[0];
                    if ("JSON".equals(serializationType)) {
                        responseBytes = testData.toJson().getBytes();
                    } else if ("XML".equals(serializationType)) {
                        responseBytes = testData.toXml().getBytes();
                    } else if ("PROTO".equals(serializationType)) {
                        responseBytes = testDataPB.toByteArray();
                    } else if ("AVRO".equals(serializationType)) {
                        responseBytes = testData.toAvro();
                    } else {
                        System.exit(1);
                    }
                    outputStream.write(responseBytes);
                    outputStream.write("sent!".getBytes());
                    long sentAt = System.nanoTime();

                    double receivingTime = (receivedAt - receivingStartedAt) / 1_000_000_000.0;
                    double processingTime = (processedAt - processingStartedAt) / 1_000_000_000.0;
                    double sendingTime = (sentAt - sendingStartedAt) / 1_000_000_000.0;
                    double totalTime = (sentAt - receivingStartedAt) / 1_000_000_000.0;

                    Map<String, String> resultMap = Map.of(
                            serverReportFieldnames[0], String.valueOf(receivingTime),
                            serverReportFieldnames[1], String.valueOf(processingTime),
                            serverReportFieldnames[2], String.valueOf(sendingTime),
                            serverReportFieldnames[3], String.valueOf(totalTime));
                    logger.logData(resultMap);
                }
                long socketEndedAt = System.nanoTime();
                double socketExecutionTime = (socketEndedAt - serverStartedAt) / 1_000_000_000.0;
                logger.logData(Map.of(serverReportFieldnames[4], String.valueOf(socketExecutionTime)));
                logger.close();
            } catch (IOException e) {
                System.err.println("Failed to accept client connection: " + e.getMessage());
            }
        } catch (IOException e) {
            System.err.println("Server startup failed: " + e.getMessage());
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    System.err.println("Failed to close server socket: " + e.getMessage());
                }
            } else {
                System.err.println("Server socket is null");
            }
        }
    }

    private boolean endsWithSubarray(byte[] mainArray, int bytesRead, byte[] subArray) {
        if (mainArray.length < subArray.length) {
            return false;
        }

        int subArrayIndex = subArray.length - 1;
        for (int i = bytesRead - 1; subArrayIndex >= 0; i--) {
            if (mainArray[i] != subArray[subArrayIndex]) {
                return false;
            }
            subArrayIndex--;
        }
        return true;
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.runServer();
    }
}
