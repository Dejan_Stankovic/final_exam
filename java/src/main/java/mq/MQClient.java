package mq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CountDownLatch;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import config.ConfigParser;
import model.TestData;
import logger.CSVLogger;
import java.util.Map;

public class MQClient {
    private ConfigParser config;

    private String HOST;
    private String PORT;
    private String QUEUE_1;
    private String QUEUE_2;
    private String EXCHANGE_1;
    private String EXCHANGE_2;
    private String ROUTING_KEY;
    private String VIRTUAL_HOST;
    private String USERNAME;
    private String PASSWORD;
    private String EXCHANGE_TYPE;

    private int NUM_REQUESTS;

    private Connection connection;
    private Channel channel_1;
    private Channel channel_2;

    private String[] clientReportFieldnames;
    private CSVLogger logger;

    private CountDownLatch latch;

    public MQClient() {
        config = new ConfigParser();
        HOST = config.getValue("rabbitmq", "HOST");
        PORT = config.getValue("rabbitmq", "PORT");
        QUEUE_1 = config.getValue("rabbitmq", "QUEUE_1");
        QUEUE_2 = config.getValue("rabbitmq", "QUEUE_2");
        EXCHANGE_1 = config.getValue("rabbitmq", "EXCHANGE_1");
        EXCHANGE_2 = config.getValue("rabbitmq", "EXCHANGE_2");
        ROUTING_KEY = config.getValue("rabbitmq", "ROUTING_KEY");
        VIRTUAL_HOST = config.getValue("rabbitmq", "VIRTUAL_HOST");
        USERNAME = config.getValue("rabbitmq", "USERNAME");
        PASSWORD = config.getValue("rabbitmq", "PASSWORD");
        EXCHANGE_TYPE = config.getValue("rabbitmq", "EXCHANGE_TYPE");

        NUM_REQUESTS = Integer.parseInt(config.getValue("client", "NUM_REQUESTS"));

        clientReportFieldnames = config.getValue("report-columns", "CLIENT").split(",");
        String className = this.getClass().getName();
        String csvFile = "reports/java/" + className + ".java.csv";
        logger = new CSVLogger(csvFile, false, clientReportFieldnames);

        latch = new CountDownLatch(1);

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        factory.setPort(Integer.parseInt(PORT));
        factory.setVirtualHost(VIRTUAL_HOST);
        factory.setUsername(USERNAME);
        factory.setPassword(PASSWORD);

        try {
            // Connect to the RabbitMQ server
            connection = factory.newConnection();
            channel_1 = connection.createChannel();
            channel_2 = connection.createChannel();

            channel_1.exchangeDeclare(EXCHANGE_1, EXCHANGE_TYPE, false, false, null);
            channel_2.exchangeDeclare(EXCHANGE_2, EXCHANGE_TYPE, false, false, null);

            channel_1.queueDeclare(QUEUE_1, false, false, false, null);
            channel_2.queueDeclare(QUEUE_2, false, false, false, null);

            channel_1.queueBind(QUEUE_1, EXCHANGE_1, ROUTING_KEY);
            channel_2.queueBind(QUEUE_2, EXCHANGE_2, ROUTING_KEY);

        } catch (

        IOException e) {
            System.out.println("IOException in constructor: " + e.getMessage());
        } catch (TimeoutException e) {
            System.out.println("TimeoutException in constructor: " + e.getMessage());
        }
    }

    private DeliverCallback deliverCallback = (consumerTag, delivery) -> {
        String message = new String(delivery.getBody(), "UTF-8");
        if ("exit!".equals(message)) {
            latch.countDown();
        }
    };

    private void consumeMessages() {
        try {
            System.out.println("Waiting for messages. To exit, press CTRL+C");

            channel_2.basicConsume(QUEUE_2, true, deliverCallback, consumerTag -> {
            });
            latch.await();
        } catch (IOException | InterruptedException e) {
            System.out.println("Exception: " + e);
        }
    }

    private void publishMessages() {
        try {
            String message;
            for (int i = 0; i < NUM_REQUESTS; i++) {
                long singleMessageCreationStartedAt = System.nanoTime();
                TestData testData = new TestData();
                testData.addRandomString();
                testData.addBigArray();
                message = testData.toJson();
                long singleMessageCreationEndedAt = System.nanoTime();

                channel_1.basicPublish(EXCHANGE_1, ROUTING_KEY, null, message.getBytes());
                long singleMessageSentAt = System.nanoTime();

                double creatingMessageTime = (singleMessageCreationEndedAt - singleMessageCreationStartedAt)
                        / 1_000_000_000.0;
                double sendingMessageTime = (singleMessageSentAt - singleMessageCreationEndedAt) / 1_000_000_000.0;
                double totalTime = (singleMessageSentAt - singleMessageCreationStartedAt) / 1_000_000_000.0;

                Map<String, String> resultMap = Map.of(
                        clientReportFieldnames[0], String.valueOf(creatingMessageTime),
                        clientReportFieldnames[1], String.valueOf(sendingMessageTime),
                        // we are not receiving any response from the server in this part
                        clientReportFieldnames[3], String.valueOf(totalTime));
                logger.logData(resultMap);
            }
            message = "exit!";
            channel_1.basicPublish(EXCHANGE_1, ROUTING_KEY, null, message.getBytes());
        } catch (IOException e) {
            System.out.println("Exception: " + e);
        }
    }

    public static void main(String[] args) {
        MQClient client = new MQClient();

        long publishingsStartedAt = System.nanoTime();

        client.publishMessages();

        client.consumeMessages();

        long consumingsEndedAt = System.nanoTime();

        double totalExecutionTime = (consumingsEndedAt - publishingsStartedAt) / 1_000_000_000.0;
        client.logger.logData(Map.of(
                client.clientReportFieldnames[4], String.valueOf(totalExecutionTime)));
        client.logger.close();
        try {
            client.connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
