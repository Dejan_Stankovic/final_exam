package mq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CountDownLatch;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import config.ConfigParser;
import model.TestData;
import logger.CSVLogger;
import java.util.Map;

public class MQServer {
    private ConfigParser config;

    private String HOST;
    private String PORT;
    private String QUEUE_1;
    private String QUEUE_2;
    private String EXCHANGE_1;
    private String EXCHANGE_2;
    private String ROUTING_KEY;
    private String VIRTUAL_HOST;
    private String USERNAME;
    private String PASSWORD;
    private String EXCHANGE_TYPE;

    private Connection connection;
    private Channel channel_1;
    private Channel channel_2;

    private String[] serverReportFieldnames;
    private CSVLogger logger;

    private CountDownLatch latch;

    public MQServer() {
        config = new ConfigParser();
        HOST = config.getValue("rabbitmq", "HOST");
        PORT = config.getValue("rabbitmq", "PORT");
        QUEUE_1 = config.getValue("rabbitmq", "QUEUE_1");
        QUEUE_2 = config.getValue("rabbitmq", "QUEUE_2");
        EXCHANGE_1 = config.getValue("rabbitmq", "EXCHANGE_1");
        EXCHANGE_2 = config.getValue("rabbitmq", "EXCHANGE_2");
        ROUTING_KEY = config.getValue("rabbitmq", "ROUTING_KEY");
        VIRTUAL_HOST = config.getValue("rabbitmq", "VIRTUAL_HOST");
        USERNAME = config.getValue("rabbitmq", "USERNAME");
        PASSWORD = config.getValue("rabbitmq", "PASSWORD");
        EXCHANGE_TYPE = config.getValue("rabbitmq", "EXCHANGE_TYPE");

        serverReportFieldnames = config.getValue("report-columns", "SERVER").split(",");
        String className = this.getClass().getName();
        String csvFile = "reports/java/" + className + ".java.csv";
        logger = new CSVLogger(csvFile, false, serverReportFieldnames);

        latch = new CountDownLatch(1);

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        factory.setPort(Integer.parseInt(PORT));
        factory.setVirtualHost(VIRTUAL_HOST);
        factory.setUsername(USERNAME);
        factory.setPassword(PASSWORD);

        try {
            // Connect to the RabbitMQ server
            connection = factory.newConnection();
            channel_1 = connection.createChannel();
            channel_2 = connection.createChannel();

            channel_1.exchangeDeclare(EXCHANGE_1, EXCHANGE_TYPE, false, false, null);
            channel_2.exchangeDeclare(EXCHANGE_2, EXCHANGE_TYPE, false, false, null);

            channel_1.queueDeclare(QUEUE_1, false, false, false, null);
            channel_2.queueDeclare(QUEUE_2, false, false, false, null);

            channel_1.queueBind(QUEUE_1, EXCHANGE_1, ROUTING_KEY);
            channel_2.queueBind(QUEUE_2, EXCHANGE_2, ROUTING_KEY);

        } catch (

        IOException e) {
            System.out.println("IOException in constructor: " + e.getMessage());
        } catch (TimeoutException e) {
            System.out.println("TimeoutException in constructor: " + e.getMessage());
        }
    }

    private DeliverCallback deliverCallback = (consumerTag, delivery) -> {
        String message = new String(delivery.getBody(), "UTF-8");
        if (!"exit!".equals(message)) {
            long processingStartedAt = System.nanoTime();
            TestData testData = TestData.fromJson(message);
            Collections.sort(testData.getArray());
            testData.setString(
                    new StringBuilder(testData.getString()).reverse().toString());
            long processedAt = System.nanoTime();

            message = testData.toJson();
            channel_2.basicPublish(EXCHANGE_2, ROUTING_KEY, null, message.getBytes());
            long sentAt = System.nanoTime();

            double processingTime = (processedAt - processingStartedAt) / 1_000_000_000.0;
            double sendingTime = (sentAt - processedAt) / 1_000_000_000.0;
            double totalTime = (sentAt - processingStartedAt) / 1_000_000_000.0;

            Map<String, String> resultMap = Map.of(
                    // we don't have receiving time here
                    serverReportFieldnames[1], String.valueOf(processingTime),
                    serverReportFieldnames[2], String.valueOf(sendingTime),
                    serverReportFieldnames[3], String.valueOf(totalTime));
            logger.logData(resultMap);
        } else {
            message = "exit!";
            channel_2.basicPublish(EXCHANGE_2, ROUTING_KEY, null, message.getBytes());
            latch.countDown();
        }
    };

    private void consumeMessages() {
        try {
            System.out.println("Waiting for messages. To exit, press CTRL+C");

            channel_1.basicConsume(QUEUE_1, true, deliverCallback, consumerTag -> {
            });
            latch.await();
        } catch (IOException | InterruptedException e) {
            System.out.println("Exception: " + e);
        }

    }

    public static void main(String[] args) {
        MQServer server = new MQServer();

        long serverConsumeStartedAt = System.nanoTime();
        server.consumeMessages();
        long serverConsumeEndedAt = System.nanoTime();

        double executionTime = (serverConsumeEndedAt - serverConsumeStartedAt) / 1_000_000_000.0;
        server.logger.logData(
                Map.of(server.serverReportFieldnames[4], String.valueOf(executionTime)));
        server.logger.close();
        try {
            server.connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
