package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.protobuf.InvalidProtocolBufferException;

import model.TestDataProtoOuterClass.TestDataProto;

import config.ConfigParser;

public class TestDataPB {
    private static ConfigParser config = new ConfigParser();
    private TestDataProto testDataProto;

    public TestDataPB() {
        this.testDataProto = TestDataProto.newBuilder().build();
    }

    public TestDataPB(byte[] messageBytes) throws InvalidProtocolBufferException {
        this.testDataProto = TestDataProto.parseFrom(messageBytes);
    }

    public String getString() {
        return this.testDataProto.getString();
    }

    public void setString(String string) {
        this.testDataProto = TestDataProto.newBuilder()
                .setString(string)
                .addAllArray(this.testDataProto.getArrayList())
                .build();
    }

    public Integer[] getArray() {
        List<Integer> array = this.testDataProto.getArrayList();
        return array.toArray(new Integer[array.size()]);
    }

    public void setArray(List<Integer> array) {
        this.testDataProto = TestDataProto.newBuilder()
                .setString(this.testDataProto.getString())
                .addAllArray(array)
                .build();
    }

    public byte[] toByteArray() {
        return this.testDataProto.toByteArray();
    }

    public void addRandomString() {
        int dataArrayLen = Integer.parseInt(config.getValue("model", "DATA_ARRAY_LEN"));
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < dataArrayLen; i++) {
            int randomIndex = random.nextInt(characters.length());
            char randomChar = characters.charAt(randomIndex);
            stringBuilder.append(randomChar);
        }

        this.testDataProto = TestDataProto.newBuilder()
                .setString(stringBuilder.toString())
                .addAllArray(this.testDataProto.getArrayList())
                .build();
    }

    public void addBigArray() {
        List<Integer> array = new ArrayList<>();
        int dataArrayLen = Integer.parseInt(config.getValue("model", "DATA_ARRAY_LEN"));
        int dataArrayMaxValue = Integer.parseInt(config.getValue("model", "DATA_ARRAY_MAX_VALUE"));
        Random random = new Random();
        for (int i = 0; i < dataArrayLen; i++) {
            array.add(random.nextInt(dataArrayMaxValue + 1));
        }

        this.testDataProto = TestDataProto.newBuilder()
                .setString(this.testDataProto.getString())
                .addAllArray(array)
                .build();
    }
}
