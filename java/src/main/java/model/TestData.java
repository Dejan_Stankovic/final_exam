package model;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.apache.avro.Schema;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.reflect.ReflectDatumReader;
import org.apache.avro.reflect.ReflectDatumWriter;

import config.ConfigParser;

public class TestData {

    private static ConfigParser config = new ConfigParser();

    private String string;
    private List<Integer> array;

    public TestData() {
        this.string = "";
        this.array = new ArrayList<>();
    }

    public String getString() {
        return string;
    }

    public List<Integer> getArray() {
        return array;
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setArray(List<Integer> array) {
        this.array = array;
    }

    public void addRandomString() {
        int dataArrayLen = Integer.parseInt(config.getValue("model", "DATA_ARRAY_LEN"));
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < dataArrayLen; i++) {
            int randomIndex = random.nextInt(characters.length());
            char randomChar = characters.charAt(randomIndex);
            stringBuilder.append(randomChar);
        }
        this.string = stringBuilder.toString();
    }

    public void addBigArray() {
        int dataArrayLen = Integer.parseInt(config.getValue("model", "DATA_ARRAY_LEN"));
        int dataArrayMaxValue = Integer.parseInt(config.getValue("model", "DATA_ARRAY_MAX_VALUE"));
        Random random = new Random();
        for (int i = 0; i < dataArrayLen; i++) {
            this.array.add(random.nextInt(dataArrayMaxValue + 1));
        }
    }

    // JSON serialization and deserialization
    public String toJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static TestData fromJson(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, TestData.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // XML serialization and deserialization
    public String toXml() {
        try {
            XmlMapper xmlMapper = new XmlMapper();
            return xmlMapper.writeValueAsString(this);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static TestData fromXml(String xml) {
        try {
            XmlMapper xmlMapper = new XmlMapper();
            return xmlMapper.readValue(xml, TestData.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // AVRO serialization and deserialization
    public byte[] toAvro() {
        try {

            Schema schema = new Schema.Parser().parse(new File("testData.avsc"));
            ReflectDatumWriter<TestData> testDataDatumWriter = new ReflectDatumWriter<TestData>(schema);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
            testDataDatumWriter.write(this, encoder);
            encoder.flush();
            byte[] bytes = out.toByteArray();
            out.close();
            return bytes;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static TestData fromAvro(byte[] avroBytes) {
        try {
            Schema schema = new Schema.Parser().parse(new File("testData.avsc"));
            ReflectDatumReader<TestData> testDataDatumReader = new ReflectDatumReader<TestData>(schema);
            InputStream inputStream = new BufferedInputStream(new java.io.ByteArrayInputStream(avroBytes));
            BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(inputStream,
                    null);
            TestData testData = testDataDatumReader.read(null, decoder);
            return testData;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
