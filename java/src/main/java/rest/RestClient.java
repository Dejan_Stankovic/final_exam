package rest;

import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

import config.ConfigParser;
import model.TestData;
import logger.CSVLogger;
import java.util.Map;

public class RestClient {

    private static ConfigParser config = new ConfigParser();
    private static String HOST = config.getValue("rest-service", "HOST");
    private static String PORT = config.getValue("rest-service", "PORT");
    private static int NUM_REQUESTS = Integer.parseInt(config.getValue("client", "NUM_REQUESTS"));
    String[] clientReportFieldnames = config.getValue("report-columns", "CLIENT").split(",");
    String className = this.getClass().getName();
    String csvFile = "reports/java/" + className + ".java.csv";
    CSVLogger logger = new CSVLogger(csvFile, false, clientReportFieldnames);

    private void runClient() {
        // Measure the time taken to send the requests
        long clientBeganAt = System.nanoTime();

        RestTemplate restTemplate = new RestTemplateBuilder().build();
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("http://")
                .append(HOST)
                .append(":")
                .append(PORT)
                .append("/api/test");
        String url = urlBuilder.toString();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        for (int i = 0; i < NUM_REQUESTS; i++) {
            long creatingMessageStartedAt = System.nanoTime();
            TestData testData = new TestData();
            testData.addRandomString();
            testData.addBigArray();
            String message = testData.toJson();
            long creatingMessageEndedAt = System.nanoTime();

            long requestStartedAt = System.nanoTime();
            HttpEntity<String> requestEntity = new HttpEntity<>(message, headers);
            restTemplate.postForEntity(url, requestEntity, String.class);
            long requestEndedAt = System.nanoTime();

            double creatingMessageTime = (creatingMessageEndedAt - creatingMessageStartedAt) / 1_000_000_000.0;
            double requestTime = (requestEndedAt - requestStartedAt) / 1_000_000_000.0;
            double totalTime = (requestEndedAt - creatingMessageStartedAt) / 1_000_000_000.0;

            Map<String, String> report = Map.of(
                    clientReportFieldnames[0], String.valueOf(creatingMessageTime),
                    // we have only one value for sending a post request and receiving a response
                    clientReportFieldnames[2], String.valueOf(requestTime),
                    clientReportFieldnames[3], String.valueOf(totalTime));
            logger.logData(report);
        }
        long clientEndedAt = System.nanoTime();
        double executionTime = (clientEndedAt - clientBeganAt) / 1_000_000_000.0;

        logger.logData(Map.of(clientReportFieldnames[4], String.valueOf(executionTime)));
        logger.close();
    }

    public static void main(String[] args) {
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.ERROR);

        RestClient client = new RestClient();
        client.runClient();
    }
}