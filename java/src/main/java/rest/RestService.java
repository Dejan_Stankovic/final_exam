package rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import config.ConfigParser;
import model.TestData;
import logger.CSVLogger;
import java.util.Map;

@RestController
public class RestService {

    @PostMapping("/api/test")
    public Object receiveMessage(@RequestBody TestData testData) {
        long processingStartedAt = System.nanoTime();
        Collections.sort(testData.getArray());
        testData.setString(
                new StringBuilder(testData.getString()).reverse().toString());
        long processedAt = System.nanoTime();

        long sendingStartedAt = System.nanoTime();
        String response = testData.toJson();

        List<Object> responseParams = new ArrayList<Object>();
        responseParams.add(response);
        responseParams.add(processingStartedAt);
        responseParams.add(processedAt);
        responseParams.add(sendingStartedAt);
        return responseParams;
    }

}

@Aspect
@Component
class TimingAspect {

    private static ConfigParser config = new ConfigParser();
    private static String[] serverReportFieldnames = config.getValue("report-columns", "SERVER").split(",");
    private static String csvFile = "reports/java/rest.RestService.java.csv";
    private static CSVLogger logger = new CSVLogger(csvFile, false, serverReportFieldnames);
    private static int NUM_REQUESTS = Integer.parseInt(config.getValue("client", "NUM_REQUESTS"));
    private static int PROCESSED_REQUESTS = 0;

    @Around("execution(* rest.RestService.receiveMessage(..))")
    public Object measureRequestTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long requestStartedAt = System.nanoTime();
        List<?> responseParams = null;
        String response = null;
        long processingStartedAt = 0;
        long processedAt = 0;
        long sendingStartedAt = 0;
        try {
            Object result = joinPoint.proceed();
            responseParams = (List<?>) result;

            long requestEndedAt = System.nanoTime();

            response = (String) responseParams.get(0);
            processingStartedAt = (long) responseParams.get(1);
            processedAt = (long) responseParams.get(2);
            sendingStartedAt = (long) responseParams.get(3);

            double receivingTime = (processingStartedAt - requestStartedAt) / 1_000_000_000.0;
            double processing_time = (processedAt - processingStartedAt) / 1_000_000_000.0;
            double sendingTime = (requestEndedAt - sendingStartedAt) / 1_000_000_000.0;
            double totalTime = (requestEndedAt - requestStartedAt) / 1_000_000_000.0;

            Map<String, String> resultMap = Map.of(
                    serverReportFieldnames[0], String.valueOf(receivingTime),
                    serverReportFieldnames[1], String.valueOf(processing_time),
                    serverReportFieldnames[2], String.valueOf(sendingTime),
                    serverReportFieldnames[3], String.valueOf(totalTime));
            logger.logData(resultMap);

            PROCESSED_REQUESTS++;
            if (PROCESSED_REQUESTS == NUM_REQUESTS) {
                logger.close();
            }
            return response;
        } catch (ClassCastException e) {
            System.out.println("ClassCastException: " + e.getMessage());
            return response;
        }
    }
}