package rest;

import java.util.Map;
import java.util.HashMap;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

import config.ConfigParser;

@SpringBootApplication
@EnableAspectJAutoProxy
public class RestApplication {
    private static ConfigParser config = new ConfigParser();
    private static String HOST = config.getValue("rest-service", "HOST");
    private static String PORT = config.getValue("rest-service", "PORT");

    public static void main(String[] args) {
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.ERROR);

        SpringApplication app = new SpringApplication(RestApplication.class);

        Map<String, Object> props = new HashMap<>();
        props.put("server.port", PORT);
        props.put("server.address", HOST);

        app.setDefaultProperties(props);
        app.run(args);
    }
}
